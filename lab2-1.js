function isInteger(num) {
    var tmp = num.toString();
    if (!isNaN(num) && (tmp.indexOf(',') == false || tmp.indexOf(',') == -1)
        && (tmp.indexOf('.') == false || tmp.indexOf('.') == -1))
        return true;
    return false;
}

function findPrimes(a, b) {
    var p = [];
    var k = 0;
    for (var i = a; i <= b; i++) {
        var f = true;
        for (var j = 2; j <= b; j++) {
            if (i != j && i % j == 0) {
                f = false;
                break;
            }
        }
        if (f) {
            p[k] = i;
            k++;
        }
    }
    console.log(p);
}