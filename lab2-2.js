function isIPAddress(ip) {
    var regexp = /^(25[0-5]|2[0-4]\d|[0-1]\d{2}|\d{2}|\d)(\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|\d{2}|\d)){3}$/gi;
    if (ip.search(regexp) >= 0) {
        return true;
    }
    return false;
}
function findRGBA(text) {
    var regexp = /rgba\((?:\s*(0|[1-9]\d?|1\d\d?|2[0-4]\d|25[0-5])\s*,){3}\s*(?:0\.[1-9]|[01])\s*\)/gi;
    if(text.search(regexp) >= 0) {
        var m = text.match(regexp);
        return m;
    }
    return null;
}
function findHexColor(text) {
    var regexp = /#[0-9a-f]{6}|[0-9a-f]{3}/gi;
    if(text.search(regexp) >= 0) {
        return text.match(regexp);
    }
    return false;
}
function findTags(text, tag) {
    var regexp = new RegExp("(\<" + tag + "(\/?[^>]*)>)(<" + tag + "\/>)?", 'gi');
    if(text.search(regexp) >= 0) {
        return text.match(regexp);
    }
    return false;
}
function findPosNum(text) {
    var regexp = /[^-][1-9]\d+(?:(?:\.|,)\d+)?/gi;
    if(text.search(regexp) >= 0) {
        return text.match(regexp);
    }
    return false;
}
function findDates(text) {
    var regexp = /[1-9]\d{3}\-(((0(4|6|9)|11))\-(0[1-9]|[12]\d|30)|(0(1|3|5|7|8)|10|12)\-(0[1-9]|[12]\d|3[01])|(02\-(0[1-9]|[12]\d)))/gi;
    if(text.search(regexp) >= 0) {
        return text.match(regexp);
    }
    return false;
}
